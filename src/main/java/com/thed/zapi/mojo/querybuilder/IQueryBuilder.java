package com.thed.zapi.mojo.querybuilder;

import java.util.Map;

import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.surefire.report.ReportTestCase;

/**
 * Interface for query builder for resolution of Zapi test bases on the test case
 * @author Gonzalo Gomez Garcia (gonzalo@arcadiaconsulting.es)
 *
 */
public interface IQueryBuilder {
  
  /**
   * Generates the query for the test resolution on ZAPI
   * @param tc The test case executiong info
   * @param log 
   * @param The params configured for the query builder
   * @return
   */
  public String buildQuery(ReportTestCase tc, Map<String, String> params, Log log) throws QueryBuilderException;

}
