package com.thed.zapi.mojo.querybuilder;

/**
 * Exception to the execution of {@link IQueryBuilder#buildQuery(org.apache.maven.plugins.surefire.report.ReportTestCase, java.util.Map, org.apache.maven.plugin.logging.Log)}
 * @author Gonzalo Gomez Garcia (gonzalo_gomezg@gexterno.es)
 *
 */
public class QueryBuilderException extends RuntimeException {

  /**
   * Default serial UID
   */
  private static final long serialVersionUID = 1L;

  public QueryBuilderException(String message, Throwable cause) {
    super(message, cause);
  }

  public QueryBuilderException(String message) {
    super(message);
  }
  
  

}
