package com.thed.zapi.mojo.querybuilder.impl;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.surefire.report.ReportTestCase;

import com.thed.zapi.mojo.querybuilder.IQueryBuilder;
import com.thed.zapi.mojo.querybuilder.QueryBuilderException;

/**
 * {@link IQueryBuilder} implementaion for building query bases for the JIRA issue code
 * The issue code is resolved from the test case name using a regular expression provided on the params {@link Parameters#testCaseNameRegualarExpression}
 * Also, the group index on the regular expression that contains the issue name shall be provided via param {@link Parameters#issueGroupIndex}
 * @author Gonzalo Gomez Garcia (gonzalo@acadiaconsulting.es)
 *
 */
public class JIRAIssueQueryBuilder implements IQueryBuilder {
  
  protected static final String QUERY_FORMAT = "issue = \"%s\"";
  
  /**
   * Enumeration taht
   * @author Gonzalo Gomez Garcia (gonzalo@acadiaconsulting.es)
   *
   */
  public enum Parameters{
    testCaseNameRegualarExpression,
    issueGroupIndex
  }

  @Override
  public String buildQuery(ReportTestCase tc, Map<String, String> params, Log log) throws QueryBuilderException {
    validateParams(params, log);
    
    log.debug(String.format("Params: %s. ReportTestCase: %s", params, tc) );
    
    Matcher matcher = Pattern.compile(params.get(Parameters.testCaseNameRegualarExpression.name())).matcher(tc.getName());
    int groupIndex = Integer.parseInt(params.get(Parameters.issueGroupIndex.name()));
    
    if(matcher.matches()) {
      if(matcher.groupCount() >= groupIndex) {
        String issue = matcher.group(groupIndex);
        if(StringUtils.isNotBlank(issue )) {
            return String.format(QUERY_FORMAT, issue.replaceAll("_", "-"));
        } else {
          log.warn(String.format("Group %s is empty. Returning null query",  groupIndex) );
        }
      } else {
        log.warn(String.format("The regexp matcher group count (%s) is smaller that the provided group indes (%s). Returning null query", 
            matcher.groupCount(), groupIndex) );
      }
    } else {
      log.warn(String.format("Tese case full name '%s' does not match given regular expression '%s'. Returning null query", 
          tc.getFullName(), Parameters.testCaseNameRegualarExpression.name()) );
    }
    
    return null;
    
  }
  
  
  /**
   * Validate the required params for the execution are correct.
   * @param params
   */
  protected void validateParams(Map<String, String> params, Log log) {
    
    if(log == null) {
      throw new QueryBuilderException("Log instance must be provided");
    }

    if(MapUtils.isEmpty(params)) {
      throw new QueryBuilderException("Parameters testCaseNameRegualarExpression and issueGroupIndex must be provided");
    } 
    
    String regExp = params.get(Parameters.testCaseNameRegualarExpression.name());
    String gropuIndexStr = params.get(Parameters.issueGroupIndex.name());
    
    if(StringUtils.isBlank(regExp) || StringUtils.isBlank(gropuIndexStr)) {
      throw new QueryBuilderException("Parameters testCaseNameRegualarExpression and issueGroupIndex must be provided");
    }
    
    if(!NumberUtils.isNumber(gropuIndexStr)) {
      throw new QueryBuilderException("issueGroupIndex must be a positive number");
    }

    int groupIndex = Integer.parseInt(gropuIndexStr);
    if(groupIndex < 0) {
      throw new QueryBuilderException("issueGroupIndex must be a positive number");
    }
    
    try {
      Pattern.compile(regExp);
    } catch(PatternSyntaxException e) {
      throw new QueryBuilderException("Invalid regular expression", e);
    }



  }
  
  
  

}
