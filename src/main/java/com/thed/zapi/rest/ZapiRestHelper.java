package com.thed.zapi.rest;

import com.google.common.collect.ImmutableMap;
import com.sun.jersey.api.client.*;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.ClientFilter;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.thed.zapi.rest.config.Configuration;
import com.thed.zapi.rest.generated.ZapiRestClient;
import com.thed.zapi.rest.vo.*;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.mockito.internal.util.collections.ListUtil;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.text.DateFormat;
import java.util.*;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.thed.zapi.rest.generated.ZapiRestClient.*;

/**
 * Created by smangal on 1/30/14.
 */
public class ZapiRestHelper {

    private final com.sun.jersey.api.client.Client client;
    private final Configuration config;
    final static Logger logger = Logger.getLogger(ZapiRestHelper.class.getName());

    public ZapiRestHelper(Configuration config) {
        this.config = config;
        ClientConfig cc = new DefaultClientConfig();
        cc.getClasses().add(JacksonJsonProvider.class);
        client = Client.create(cc);
        final HTTPBasicAuthFilter authFilter = new HTTPBasicAuthFilter(config.getUserName(), config.getPassword());
        client.addFilter(authFilter);
        client.addFilter(new ClientFilter() {
            private ArrayList<Object> cookies;

            @Override
            public ClientResponse handle(ClientRequest request) throws ClientHandlerException {
                if (cookies != null) {
                    request.getHeaders().put("Cookie", cookies);
                }
                ClientResponse response = getNext().handle(request);
                if (response.getCookies() != null) {
                    if (cookies == null) {
                        cookies = new ArrayList<Object>();
                    }
                    // simple addAll just for illustration (should probably check for duplicates and expired cookies)
                    cookies.addAll(response.getCookies());
                }
                return response;
            }
        });
        String zapiInfo = ZapiRestClient.systemInfo(client, config.getZapiUrl()).getAsJson(String.class);
        logger.info(zapiInfo);
        client.removeFilter(authFilter);
    }
    
    

    /**
     * Creates a new Cycle
     * If clyclePolicy == 0 or clyclePolicy == -1, first tries to find a cycle with the given cyclePrefix
     * @return
     * @throws JSONException
     * @param cyclePrefix
     */
    public Long createCycle(String cyclePrefix) throws JSONException {
      
        Long cycleId =  null;
        Cycle cycle = cycle(client, config.getZapiUrl());
        if(config.getCyclePolicy() < 1) {
          Map<String, Object> response = cycle.getAsJson(Long.parseLong(config.getProjectId()), Long.parseLong(config.getVersionId()), null, null, null, null, new GenericType<Map<String, Object>>(){});
          if(MapUtils.isNotEmpty(response)) {
            for (Map.Entry<String, Object> entry: response.entrySet()) {
              if(entry.getValue() instanceof Map) {
                Map<String, Object> cycleVO = (Map<String, Object>)entry.getValue();
                if(StringUtils.equals((String)cycleVO.get("name"), cyclePrefix)) {
                  cycleId = Long.parseLong(entry.getKey());
                  break;
                }
              }
            }
          } else {
            logger.warning("Cycle with nane %s not found. New one will be created");
          }
        } 
        
        
        if(cycleId == null) {
          //JSONObject cycleJSON = new JSONObject(ImmutableMap.of("name", "Cycle" + new Date(), "projectId", config.getProjectId(), "versionId", config.getVersionId(), "description","Creation from ZAPI Maven Plugin"));
          CycleVO cycleObj = new CycleVO(null, null, cyclePrefix + " " + DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT).format(new Date()), "Build", "Created from ZAPI Maven Plugin", "Env", config.getVersionId(), config.getProjectId(), null, null);
          GenericResponseVO cResult = cycle.postJson(cycleObj, new GenericType<GenericResponseVO>() {});
          
          cycleId=  cResult.getId();
        } 
        
        return cycleId;
        
    }

    public void deleteCycle(Long id) throws JSONException {
        Cycle cycle = cycle(client, config.getZapiUrl());
        /*{"success":"Cycle 9 successfully deleted"}*/
        String cResult = cycle.id(id).deleteAsJson(String.class);
        logger.log(Level.ALL, cResult);
    }

    public void executeTest(Integer cycleId, Integer testcaseId, String result, String stackTrace) throws JSONException, IOException {
        Execution executionService = execution(client, config.getZapiUrl());
        ExecutionResponseVO executionRes = executionService.getAsJson(testcaseId, null, null, cycleId, 0, null, null, ExecutionResponseVO.class);
        String updateResponse;
        ExecutionVO executionObj = null;
        
        boolean createNewExecution = true;
        if(executionRes != null && executionRes.recordsCount > 0){
          if(config.getCyclePolicy() == -1) {
            final List<String> executionIds = new ArrayList<String>();
            executionRes.executions.forEach(new Consumer<ExecutionVO>() {
              @Override
              public void accept(ExecutionVO t) {
                executionIds.add(String.valueOf(t.id));
              }
            });
            String deleteResponse = executionService.deleteExecutions().deleteJson(new DeleteBulkExecutionVO(executionIds), String.class);
            logger.log(Level.ALL, String.format("Delete executionIds: %s. %s", executionIds, deleteResponse));
          } else {
            executionObj = executionRes.executions.get(0);
            createNewExecution = false;
          }
        }
            
        if (createNewExecution){
            executionObj = new ExecutionVO(result, cycleId, config.getProjectId(), config.getVersionId(), testcaseId.toString(), stackTrace);
            Map<String, ExecutionVO> executionVOMap = executionService.postJson(executionObj, new GenericType<Map<String, ExecutionVO>>(){});
            executionObj = executionVOMap.entrySet().iterator().next().getValue();
        }
        executionObj.executionStatus = result;
        stackTrace = StringUtils.substring(stackTrace, 0, 750);
        Map<String, String> updateVals = ImmutableMap.<String, String>of("status", result, "comment", stackTrace, "executedOn", "Today 1:05 PM");
        updateResponse = executionService.idExecute(executionObj.id).putJson(new ObjectMapper().writeValueAsString(updateVals), String.class);
        
        
        if(StringUtils.isNotBlank(stackTrace)) {
           Attachment attachment = attachment(client, config.getZapiUrl());
           attachment.postAsJson(executionObj.id, "EXECUTION", stackTrace, AttachmentResponseVO.class);
        }
        
        logger.log(Level.ALL, updateResponse);
    }

    public List<StatusVO> getExecutionStatuses(){
        Util.TestExecutionStatus statusUtil = util(client, config.getZapiUrl()).testExecutionStatus();
        List<StatusVO> value = statusUtil.getAsJson(new GenericType<List<StatusVO>>() {});
        return value;
    }

    public List<StatusVO> getStepStatuses(){
        Util.TeststepExecutionStatus statusUtil = util(client, config.getZapiUrl()).teststepExecutionStatus();
        List<StatusVO> value = statusUtil.getAsJson(new GenericType<List<StatusVO>>() {});
        return value;
    }

    public Long findOrAddTestcase(String fieldName, String fieldVal, String query) throws JSONException, IOException {
      
        if(StringUtils.isBlank(query)) {
          query = fieldName + "~\"" + fieldVal +"\"";
        }
        String issueRes = getAsJson(new GenericType<String>(){}, config.getJiraUrl(), "search", ImmutableMap.<String, Object>of("jql", query));
        JSONArray issueObjects = new JSONObject(issueRes).getJSONArray("issues");
        if(issueObjects != null && issueObjects.length() > 0){
            return issueObjects.getJSONObject(0).getLong("id");
        }else{
            Map<String, Object> fields = ImmutableMap.<String, Object>of("project", ImmutableMap.of("id", config.getProjectId()), fieldName, fieldVal, "issuetype", ImmutableMap.of("name", "Test"));
            Map<String, Object> issueMap = ImmutableMap.<String, Object>of("fields", fields);
            final String input = new ObjectMapper().writeValueAsString(issueMap);
            IssueResponseVO issue = postJson(new GenericType<IssueResponseVO>(){}, config.getJiraUrl(), "issue", input);
            return issue.id;
        }
    }

    private<T >T postJson(com.sun.jersey.api.client.GenericType<T> returnType, URI basePath, String apiName, Object input) {
        HashMap<String,Object> _templateAndMatrixParameterValues = new HashMap<String, Object>();
        UriBuilder localUriBuilder = UriBuilder.fromUri(basePath).path(apiName);
        com.sun.jersey.api.client.WebResource resource = client.resource(localUriBuilder.buildFromMap(_templateAndMatrixParameterValues));
        com.sun.jersey.api.client.WebResource.Builder resourceBuilder = resource.getRequestBuilder();
        resourceBuilder = resourceBuilder.accept("application/json");
        resourceBuilder = resourceBuilder.type("application/json");
        com.sun.jersey.api.client.ClientResponse response;
        response = resourceBuilder.method("POST", com.sun.jersey.api.client.ClientResponse.class, input);
        if (response.getStatus()>= 400) {
            throw new WebApplicationException(Response.status(response.getClientResponseStatus()).build());
        }
        return response.getEntity(returnType);
    }

    private<T >T getAsJson(com.sun.jersey.api.client.GenericType<T> returnType, URI basePath, String apiName, Map<String, Object> queryParams) {
        HashMap<String,Object> _templateAndMatrixParameterValues = new HashMap<String, Object>();
        UriBuilder localUriBuilder = UriBuilder.fromUri(basePath).path(apiName);
        for (Map.Entry<String, Object> params : queryParams.entrySet()) {
            localUriBuilder.replaceQueryParam(params.getKey(), params.getValue());
        }

        com.sun.jersey.api.client.WebResource resource = client.resource(localUriBuilder.buildFromMap(_templateAndMatrixParameterValues));
        com.sun.jersey.api.client.WebResource.Builder resourceBuilder = resource.getRequestBuilder();
        resourceBuilder = resourceBuilder.accept("application/json");
        com.sun.jersey.api.client.ClientResponse response;
        response = resourceBuilder.method("GET", com.sun.jersey.api.client.ClientResponse.class);
        if (response.getStatus()>= 400) {
            throw new WebApplicationException(Response.status(response.getClientResponseStatus()).build());
        }
        return response.getEntity(returnType);
    }
}
