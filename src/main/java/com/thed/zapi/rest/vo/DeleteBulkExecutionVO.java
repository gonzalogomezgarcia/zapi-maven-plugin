package com.thed.zapi.rest.vo;


import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import edu.emory.mathcs.backport.java.util.Arrays;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

/**
 * 
 * @author Gonzalo Gomez Garcia (gonzalo@arcadiaconsulting.es)
 *
 */
public class DeleteBulkExecutionVO {
  
  @XmlElement(nillable=false)
  public List<String> executions; 
  
  public DeleteBulkExecutionVO() {
  }
  
  public DeleteBulkExecutionVO(String...executionId) {
    this.executions = Arrays.asList(executionId);
  }
  
  public DeleteBulkExecutionVO(List<String> executionIds) {
    this.executions = executionIds;
  }
  


}
