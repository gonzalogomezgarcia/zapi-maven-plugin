package com.thed.zapi.rest.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AttachmentResponseVO {
  
  public String success;

  @Override
  public String toString() {
    return "AttachmentResponseVO [success=" + success + "]";
  }
  
  

}
