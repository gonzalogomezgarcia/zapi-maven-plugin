package com.thed.zapi.mojo.querybuilder.impl;

import static org.junit.Assert.*;
import static com.thed.zapi.mojo.querybuilder.impl.JIRAIssueQueryBuilder.Parameters;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.surefire.report.ReportTestCase;
import org.apache.maven.plugins.surefire.report.SurefireReportParser;
import org.apache.maven.reporting.MavenReportException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.google.common.collect.ImmutableMap;
import com.thed.zapi.mojo.querybuilder.QueryBuilderException;

public class JIRAIssueQueryBuilderTest {
  
  protected JIRAIssueQueryBuilder qb;
  
  protected  Log log;
  
  @Before
  public void init(){
    this.qb = new JIRAIssueQueryBuilder();
    this.log = Mockito.mock(Log.class);
  }

  @Test(expected=QueryBuilderException.class)
  public void testValidateParamsNull() {
    qb.validateParams(null, this.log);
  }
  
  @Test(expected=QueryBuilderException.class)
  public void testValidateParamsEmpty() {
    qb.validateParams(new HashMap<String, String>(), this.log);
  }
  
  @Test(expected=QueryBuilderException.class)
  public void testValidateParamsMissingGroup() {
    qb.validateParams(ImmutableMap.<String, String>of(Parameters.testCaseNameRegualarExpression.name(), ".*"), this.log);
  }
  
  @Test(expected=QueryBuilderException.class)
  public void testValidateParamsMissingRegExp() {
    qb.validateParams(ImmutableMap.<String, String>of(Parameters.issueGroupIndex.name(), "1"), this.log);
  }

  @Test(expected=QueryBuilderException.class)
  public void testValidateParamsInvlaidRegularExpresion() {
    qb.validateParams(ImmutableMap.<String, String>of(Parameters.testCaseNameRegualarExpression.name(), "[this is an invalid regular expression", Parameters.issueGroupIndex.name(), "1"), this.log);
  }

  @Test(expected=QueryBuilderException.class)
  public void testValidateParamsInvlaidGroup() {
    qb.validateParams(ImmutableMap.<String, String>of(Parameters.testCaseNameRegualarExpression.name(), ".*", Parameters.issueGroupIndex.name(), "this is not a nubmer"), this.log);
  }

  @Test(expected=QueryBuilderException.class)
  public void testValidateParamsNonPositiveGroup() {
    qb.validateParams(ImmutableMap.<String, String>of(Parameters.testCaseNameRegualarExpression.name(), ".*", Parameters.issueGroupIndex.name(), "-1"), this.log);
  }
  
  @Test
  public void testbuildQuery() throws MavenReportException {
    
    SurefireReportParser report = new SurefireReportParser(Arrays.asList(new File("target/test-classes/failsafe-reports")), Locale.getDefault());
    ReportTestCase tc = report.parseXMLReportFiles().get(0).getTestCases().get(0);
    
    Map<String, String> params = ImmutableMap.<String, String>of(
        Parameters.testCaseNameRegualarExpression.name(), "is not going to match", Parameters.issueGroupIndex.name(), "1");
    assertNull(this.qb.buildQuery(tc, params, log));
    
    params = ImmutableMap.<String, String>of(
        Parameters.testCaseNameRegualarExpression.name(), "ZAPI_1_testSayHello(ZAPI_[0-9]+)?", Parameters.issueGroupIndex.name(), "1");
    assertNull(this.qb.buildQuery(tc, params, log));

    params = ImmutableMap.<String, String>of(
        Parameters.testCaseNameRegualarExpression.name(), "(ZAPI_[0-9]+)_testSayHello", Parameters.issueGroupIndex.name(), "2");
    assertNull(this.qb.buildQuery(tc, params, log));
    
    params = ImmutableMap.<String, String>of(
        Parameters.testCaseNameRegualarExpression.name(), "(ZAPI_[0-9]+)_testSayHello", Parameters.issueGroupIndex.name(), "1");
    assertEquals("issue = \"ZAPI-1\"", this.qb.buildQuery(tc, params, log));

    
  }
  

}
